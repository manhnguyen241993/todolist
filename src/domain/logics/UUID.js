const getUUID = () => {
  let id = 0;
  return () => id++;
};

const uuid = getUUID();
export default uuid;
