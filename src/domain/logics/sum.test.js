import sum from "./sum";

describe("テスト サンプル", () => {
  it("1足す2は3になる", () => {
    expect(sum(1, 2)).toBe(3);
  });
});
