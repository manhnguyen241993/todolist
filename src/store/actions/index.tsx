import { ITask } from "../../model/Task";

export const addTask = (newTask: ITask) => {
  return {
    type: "ADD_ITEM",
    payload: {
      task: newTask
    }
  } as const;
};
export const deleteTask = (id: number) => {
  return {
    type: "DELETE_ITEM",
    payload: { id }
  } as const;
};
export const editTask = (id: number, newTask: string) => {
  return {
    type: "EDIT_ITEM",
    payload: { id, description: newTask }
  } as const;
};
