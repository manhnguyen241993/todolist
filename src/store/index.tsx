import { createStore } from "redux";
import allReducers from "./reducers";
import { ITask } from "../model/Task";

export interface AppState {
  tasks: ITask[];
}

const INITIAL_STATE: AppState = {
  tasks: []
};

const myStore = createStore(
  allReducers,
  INITIAL_STATE,
  (window as any).devToolsExtension && (window as any).devToolsExtension()
);
export { myStore };
