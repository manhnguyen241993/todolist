import { ITask } from "../../model/Task";
import { Action } from "../actions/types";

const taskReducer = (state: ITask[] = [], action: Action) => {
  switch (action.type) {
    case "ADD_ITEM":
      return [...state, action.payload.task];
    case "DELETE_ITEM":
      return state.filter(task => task.id !== action.payload.id);
    case "EDIT_ITEM":
      return state.map(task =>
        task.id === action.payload.id
          ? { id: action.payload.id, description: action.payload.description }
          : task
      );
    default:
      return state;
  }
};

export default taskReducer;
