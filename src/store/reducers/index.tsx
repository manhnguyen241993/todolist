import { combineReducers } from "redux";
import taskReducer from "./taskReducer";

const allReducers = combineReducers({
  tasks: taskReducer
});

export default allReducers;
