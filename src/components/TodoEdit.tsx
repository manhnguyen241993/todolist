import React, { useState } from "react";
import { StyleTodoInput } from "./styles/StyleList";

export interface TaskEditProps {
  text: string;
  onSave: any;
}
const TodoEdit = ({ text, onSave }: TaskEditProps) => {
  const [propTask, setPropTask] = useState<string>(text);

  const handleTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPropTask(event.target.value);
  };
  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.keyCode === 13) {
      onSave(event.currentTarget.value);
    }
  };
  const handleBlur = (event: React.MouseEvent<HTMLInputElement>) => {
    onSave(event.currentTarget.value);
  };
  return (
    <StyleTodoInput
      onChange={handleTextChange}
      onKeyDown={handleKeyDown}
      type="text"
      onBlur={handleBlur}
      autoFocus={true}
      value={propTask}
    />
  );
};

export default TodoEdit;
