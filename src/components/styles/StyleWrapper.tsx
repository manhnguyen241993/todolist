import styled from "styled-components";

export const Wrapper = styled.div`
   {
    padding: 20px;
    max-width: 500px;
    margin-right: auto;
    margin-left: auto;
    font-family: "Noto Sans JP", sans-serif;
    font-weight: 400;
  }
`;
