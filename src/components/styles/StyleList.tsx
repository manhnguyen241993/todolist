import styled from "styled-components";

export const StyleTodoList = styled.ul`
   {
    list-style-type: none;
    padding-left: 0;
    margin-left: 0;
  }
`;
export const StyleTodoLi = styled.li`
   {
    border-bottom: 1px solid #000;
    margin-bottom: 15px;
    padding-bottom: 10px;
    display: flex;
    align-items: center;
    > div {
      width: 100%;
      display: flex;
      align-items: center;
      height: 30px;
    }
    label {
      width: 100%;
      display: block;
    }
  }
`;
export const StyleTodoInput = styled.input`
   {
    padding: 0 5px;
    font-size: 16px;
    background: #eeeeee;
    -webkit-appearance: none;
    outline: 0;
    border: none;
    width: 100%;
    align-self: stretch;
  }
`;
export const StyleTodoDelete = styled.button`
   {
    -webkit-appearance: none;
    outline: 0;
    cursor: pointer;
    background-color: transparent;
    border: 1px solid red;
    border-radius: 50%;
    width: 20px;
    height: 20px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    margin-right: 10px;
    text-decoration: none;
    color: red;
  }
  &:before {
    content: "X";
    font-size: 12px;
  }
`;
