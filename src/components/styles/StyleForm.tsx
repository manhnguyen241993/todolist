import styled from "styled-components";

export const StyleTodoForm = styled.form`
   {
    display: flex;
    justify-content: center;
  }
`;

export const StyleInputForm = styled.input`
   {
    width: 100%;
    border: 1px solid #ccc;
    padding: 10px;
    font-size: 14px;
    outline: 0;
  }
`;

export const StyleSubmitForm = styled.div`
   {
    -webkit-appearance: none;
    background-color: slateblue;
    color: #fff;
    border-radius: 10px;
    font-size: 14px;
    outline: 0;
    padding: 2px 15px;
    white-space: nowrap;
    margin-left: 10px;
    border: 0;
    cursor: pointer;
    &[disabled] {
      opacity: 0.7;
      cursor: not-allowed;
    }
  }
`;
