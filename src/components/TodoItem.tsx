import * as React from "react";
import { StyleTodoDelete, StyleTodoLi } from "./styles/StyleList";
import { ITask } from "../model/Task";
import TodoEdit from "./TodoEdit";
import { useState } from "react";

export interface TaskItemProps {
  tasks: ITask;
  onDeleteTask: (id: number) => void;
  onEditTask: (id: number, newTask: string) => void;
}

const TodoItem = ({ tasks, onDeleteTask, onEditTask }: TaskItemProps) => {
  const [editing, setEditing] = useState<boolean>(false);
  const { description, id } = tasks;

  const handleSave = (taskId: number, text: string) => {
    if (text.length === 0) {
      onDeleteTask(id);
    } else {
      onEditTask(taskId, text);
    }
    setEditing(false);
  };
  const handleClickDelete = () => {
    onDeleteTask(id);
  };
  const handleDoubleClick = () => {
    setEditing(true);
  };

  const elementTask = () => {
    return (
      <div>
        <label onDoubleClick={handleDoubleClick}>
          Task: <span>{description}</span>
        </label>
      </div>
    );
  };
  const elementEditTask = () => {
    return (
      <div>
        Task:
        <TodoEdit
          text={description}
          onSave={(text: string) => handleSave(id, text)}
        />
      </div>
    );
  };
  return (
    <StyleTodoLi>
      <StyleTodoDelete onClick={handleClickDelete} />
      {editing ? elementEditTask() : elementTask()}
    </StyleTodoLi>
  );
};
export default TodoItem;
