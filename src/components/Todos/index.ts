import { AppState } from "../../store";
import { Dispatch } from "redux";
import { ITask } from "../../model/Task";
import { addTask, deleteTask, editTask } from "../../store/actions";
import { connect } from "react-redux";
import { Todos } from "./Todos";

const mapStateToProps = (state: AppState) => ({
  tasks: state.tasks
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onAddTask: (newTask: ITask) => dispatch(addTask(newTask)),
  onDeleteTask: (id: number) => dispatch(deleteTask(id)),
  onEditTask: (id: number, newTask: string) => dispatch(editTask(id, newTask))
});

export default connect(mapStateToProps, mapDispatchToProps)(Todos);
