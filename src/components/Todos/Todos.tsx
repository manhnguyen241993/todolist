import * as React from "react";
import { Wrapper } from "../styles/StyleWrapper";
import Header from "../Header";
import TodoForm from "../TodoForm";
import TodoList from "../TodoList";
import { ITask } from "../../model/Task";

export interface Props {
  tasks: ITask[];
  onAddTask: (newTask: ITask) => void;
  onDeleteTask: (id: number) => void;
  onEditTask: (id: number, newTask: string) => void;
}

export const Todos = ({
  tasks,
  onAddTask,
  onDeleteTask,
  onEditTask
}: Props) => {
  return (
    <Wrapper>
      <Header />
      <TodoForm onAddTask={onAddTask} />
      <TodoList
        tasks={tasks}
        onDeleteTask={onDeleteTask}
        onEditTask={onEditTask}
      />
    </Wrapper>
  );
};
