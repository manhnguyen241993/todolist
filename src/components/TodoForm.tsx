import React, { useState } from "react";
import {
  StyleInputForm,
  StyleSubmitForm,
  StyleTodoForm
} from "./styles/StyleForm";
import { ITask } from "../model/Task";
import uuid from "../domain/logics/UUID";

export interface TaskFormProps {
  onAddTask: (newTask: ITask) => void;
}

const TodoForm = (props: TaskFormProps) => {
  const [currentTask, setCurrentTask] = useState<string>("");

  const inputChange = (event: React.FormEvent<HTMLInputElement>) => {
    setCurrentTask(event.currentTarget.value);
  };
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    props.onAddTask({
      id: uuid(),
      description: currentTask
    });
    setCurrentTask("");
  };
  return (
    <StyleTodoForm onSubmit={handleSubmit}>
      <StyleInputForm type="text" onChange={inputChange} value={currentTask} />
      <StyleSubmitForm as="button" disabled={!currentTask}>
        追加
      </StyleSubmitForm>
    </StyleTodoForm>
  );
};

export default TodoForm;
