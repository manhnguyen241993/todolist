import * as React from "react";
import { StyleTodoList } from "./styles/StyleList";
import { ITask } from "../model/Task";
import TodoItem from "./TodoItem";

export interface TaskListProps {
  tasks: ITask[];
  onDeleteTask: (id: number) => void;
  onEditTask: (id: number, newTask: string) => void;
}
const TodoList = ({ tasks, onDeleteTask, onEditTask }: TaskListProps) => {
  const renderTasks = () => {
    return tasks.map((task: ITask) => {
      return (
        <TodoItem
          key={task.id}
          tasks={task}
          onDeleteTask={onDeleteTask}
          onEditTask={onEditTask}
        />
      );
    });
  };
  return <StyleTodoList>{renderTasks()}</StyleTodoList>;
};

export default TodoList;
