import React from "react";
import styled from "styled-components";

const StyleHeader = styled.h1`
   {
    color: #000;
    text-align: center;
  }
`;

const Header = () => <StyleHeader>TodoList</StyleHeader>;

export default Header;
