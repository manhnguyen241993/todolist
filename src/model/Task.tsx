// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export interface ITask {
  id: number;
  description: string;
}
