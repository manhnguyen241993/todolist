import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { myStore } from "../store";
import Todos from "../components/Todos/";

ReactDOM.render(
  <Provider store={myStore}>
    <Todos />
  </Provider>,
  document.getElementById("app") as HTMLElement
);
